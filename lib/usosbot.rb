require 'yaml'
module Usosbot
  autoload :Database, 'mark'
  autoload :Comparator, 'comparator'
  autoload :Parser, 'parser'
  autoload :Notifier, 'notifier'
  class UJBot
    
    def self.process(settings_file)
      settings = YAML.load(File.open(settings_file))
      @parser = Parser.new(
        'https://login.uj.edu.pl/login?service=https%3A%2F%2Fwww.usosweb.uj.edu.pl%2Fkontroler.php%3F_action%3Dactionx%3Alogowaniecas%2Findex%28%29&locale=pl',
        settings['login'],settings['password'])
      Database.new(settings['db_path'])
      diffs = Comparator.differences(@parser.marks)
      #pp diffs
      notifier = Notifier.new(diffs)
      notifier.send_email(settings)
    end
  end
end