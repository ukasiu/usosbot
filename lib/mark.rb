# encoding: UTF-8

require 'data_mapper'
require 'dm-sqlite-adapter'

module Usosbot
  class Database
    class Mark
      include DataMapper::Resource

      property :id, Serial
      property :subject, String
      property :type, String
      property :mark, String
      def full_name
        "#{subject} – #{type}"
      end
    end
    def initialize(db_path)
      DataMapper.setup(:default, "sqlite://#{db_path}")
      DataMapper.finalize
      DataMapper.auto_upgrade!
    end
  end
end

