require 'nokogiri'
require 'mechanize'
module Usosbot

  # http://stackoverflow.com/questions/8749101/parse-html-table-using-nokogiri-and-mechanize
  class TableExtractor  
    def self.extract_data html
      html.xpath('//*[@id="layout-c22"]/div/table[3]//tr').collect do |row|
        next if row.xpath('td').count < 2
        subject = row.at('td[1]/a').text.strip
        row.xpath('td[3]//div').map do |markdiv|
          type = markdiv.at('a').text.strip
          mark = markdiv.at('span').text.strip
          {:subject => subject, :mark => mark, :type => type}
        end
      end.flatten.compact
    end
  end

  class Parser

    attr_accessor :agent

    def initialize(instance_url,login,password)
      @agent = Mechanize.new
      @instance_url = instance_url
      @login = login
      @password = password

      self.login
    end

    def login
      login_page = @agent.get @instance_url

      login_form = login_page.forms.first
      login_form.username = @login
      login_form.password = @password
      usos_welcome_page = @agent.submit(login_form, login_form.buttons.first)
    end

    def marks
      marks_page = @agent.get('https://www.usosweb.uj.edu.pl/kontroler.php?_action=actionx:dla_stud/studia/oceny/index()')
      marks_table_dom = marks_page.search('//table')
      marks_table = TableExtractor.extract_data marks_table_dom
    end
  end
end